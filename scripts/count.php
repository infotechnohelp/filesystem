<?php

require_once "vendor/autoload.php";
require_once "config/bootstrap.php";

use Infotechnohelp\Filesystem\Filesystem;

list($script, $path) = $argv;

echo Filesystem::countDirectoryItems($path, false) . "\n";