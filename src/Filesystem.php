<?php

namespace Infotechnohelp\Filesystem;

// @todo Get rid of static methods ($baseDir = $dataDir ? DATA_DIR : ROOT_DIR;)
class Filesystem
{

    private $root;

    public function __construct($root = null)
    {
        $this->root = $root;
    }

    public function setRoot($root = null)
    {
        $this->root = $root;
    }

    public function getDir(string $filePath): string
    {
        $pathArray = explode("/", $filePath);
        array_pop($pathArray);
        return implode("/", $pathArray);
    }

    public function write(string $filePath, string $contents, bool $append = false, bool $createDir = true): void
    {
        $flag = $append ? FILE_APPEND : null;

        $filePath = "{$this->root}$filePath";

        $dir = $this->getDir($filePath);

        if ($createDir && !file_exists($dir) && !empty($dir)) {
            mkdir($dir, 0777, true);
        }

        file_put_contents($filePath, $contents, $flag);
    }

    public function read(string $filePath): string
    {
        return file_get_contents("{$this->root}$filePath");
    }

    public static function require_once(string $filePath, bool $dataDir = true)
    {
        $baseDir = $dataDir ? DATA_DIR : ROOT_DIR;

        return require_once("$baseDir$filePath");
    }

    public static function include_once(string $filePath, bool $dataDir = true)
    {
        $baseDir = $dataDir ? DATA_DIR : ROOT_DIR;

        return include_once "$baseDir$filePath";
    }

    public static function getLatestFile(string $dirPath, bool $dataDir = true): string
    {
        $baseDir = $dataDir ? DATA_DIR : ROOT_DIR;

        $files = [];

        foreach (array_filter(glob("$baseDir$dirPath/*"), 'is_file') as $file) {
            $files[] = basename($file);
        }

        return "$dirPath/" . end($files);
    }

    public function writeJson(string $filePath, $contents, bool $append = false, bool $createDir = true): void
    {
        if (!is_array($contents) && !is_object($contents)) {
            throw new \Exception("\$contents parameter might be either array or an JsonSerializable object");
        }

        $this->write($filePath, json_encode($contents), $append, $createDir);
    }

    public function readJson(string $filePath): array
    {
        return json_decode($this->read($filePath), true);
    }

    public static function readTxtList(string $filePath, bool $dataDir = true): array
    {
        $result = [];

        $rawArray = explode(PHP_EOL, self::read($filePath, $dataDir));

        foreach ($rawArray as $row) {
            $item = trim($row);

            if (empty($item)) {
                continue;
            }

            $result[] = $item;
        }

        return $result;
    }

    // @todo $recursive
    public function iterateDirectoryFiles(string $dirPath, callable $callable/*, bool $recursive = false*/)
    {
        if (!file_exists("{$this->root}$dirPath")) {
            throw new \Exception("Directory '{$this->root}$dirPath' does not exist");
        }

        foreach (array_filter(glob("{$this->root}$dirPath/*"), 'is_file') as $filePath) {
            $callable($filePath);
        }
    }

    public function iterateDirectorySubdirectories(
        string $dirPath,
        callable $callable,
        bool $recursive = false,
        bool $ignoreNonExisting = false
    )
    {
        if (!file_exists("{$this->root}$dirPath") && !$ignoreNonExisting) {
            throw new \Exception("Directory '{$this->root}$dirPath' does not exist");
        }

        foreach (array_filter(glob("{$this->root}$dirPath/*"), 'is_dir') as $dirPath) {

            $callable($dirPath);

            if ($recursive) {
                $this->iterateDirectorySubdirectories($dirPath, $callable, $recursive, $ignoreNonExisting);
            }
        }
    }

    public function rmdirRecursive(string $path)
    {
        $pathArray = explode(DIRECTORY_SEPARATOR, $path);

        $stop = false;

        while (!$stop) {
            $generatedPath = implode(DIRECTORY_SEPARATOR, $pathArray);

            if ($this->countDirectoryItems($generatedPath) > 0) {
                $stop = true;
                return;
            }

            rmdir($generatedPath);
            array_pop($pathArray);
        }
    }

    public function fileExists(string $path): bool
    {
        return file_exists("{$this->root}$path");
    }

    public function countDirectoryItems(string $path)
    {
        $files = scandir("{$this->root}$path");
        return count($files) - 2;
    }

    public static function rename(string $path, string $newPath, bool $dataDir = true)
    {
        $baseDir = $dataDir ? DATA_DIR : ROOT_DIR;

        rename("$baseDir$path", "$baseDir$newPath");
    }
}